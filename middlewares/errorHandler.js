const errorHandler = (err, req, res, next) => {
    console.log(err);
    if (err.name === 'NotFound') {
        res.status(404).json({ message: 'Task Not Found' })
    }
    else {
        res.status(500).json({ message: 'Internal Server Error' })
    }
}

module.exports = errorHandler;