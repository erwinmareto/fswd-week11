# Homework - Unit Testing & Development
Nama: Erwin Mareto Wikas
<br>
Full Stack Web Development Batch 3
<br>
Week 11
<br>
Unit Testing & Development


<br>


For unit testing in docker run the following:
```
npm run dockertest
```
soalnya kalo di local saya run _NODE_ENV=test jest --detectOpenHandles --runInBand --forceExit --silent --coverage --verbose --watchAll_  dapet error:
```
'NODE_ENV' is not recognized as an internal or external command,
operable program or batch file.
```

jadi, saya pisah scriptnya buat unit testing local sama di docker:
```
"test": "jest --detectOpenHandles --runInBand --forceExit --coverage --verbose --watchAll",

"dockertest": "NODE_ENV=test jest --detectOpenHandles --runInBand --forceExit --silent --coverage --verbose --watchAll"
```

<br>

```
CUSTOM_PORT=5432     #.env
```
unit testing di saya error kalo CUSTOM_PORT bukan 5432 


kalo CUSTOM_PORT=5439 dapet error:
```
SequelizeConnectionError: password authentication failed for user "postgres"

      at Client._connectionCallback (node_modules/sequelize/src/dialects/postgres/connection-manager.js:191:24)
      at Client._handleErrorWhileConnecting (node_modules/pg/lib/client.js:327:19)
      at Client._handleErrorMessage (node_modules/pg/lib/client.js:347:19)
      at node_modules/pg/lib/connection.js:117:12
      at Parser.parse (node_modules/pg-protocol/src/parser.ts:104:9)
      at Socket.<anonymous> (node_modules/pg-protocol/src/index.ts:7:48)
```

# CI/CD is not working