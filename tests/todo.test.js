const app = require("../app.js");
const request = require("supertest");
const { sequelize } = require("../models");
const { queryInterface } = sequelize;

beforeAll((done) => {
  queryInterface
    .bulkInsert(
      "ToDoLists",
      [
        {
          id: 1,
          title: "Task 1",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          title: "Task 2",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          title: "Task 3",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          title: "Task 4",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 5,
          title: "Task 5",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 6,
          title: "Task 6",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 7,
          title: "Task 7",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 8,
          title: "Task 8",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 9,
          title: "Task 9",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 10,
          title: "Task 10",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    )
    .then((_) => {
      done();
    })
    .catch((err) => {
      console.log(err);
      done(err);
    });
});

afterAll((done) => {
  queryInterface
    .bulkDelete("ToDoLists", null, {})
    .then((_) => {
      done();
    })
    .catch((err) => {
      console.log(err);
      done(err);
    });
});

describe("Testing Todolist", () => {
  it("Retrieve success message from todo list API", (done) => {
    request(app)
      .get("/todo")
      .expect("Content-Type", /json/)
      .then((response) => {
        const { body, status } = response;
        expect(status).toEqual(200);
        expect(body.length).toEqual(10);

        const task1 = body[0];
        expect(task1.title).toBe("Task 1");
        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  test("Retrieve task using ID", (done) => {
    request(app)
      .get("/todo/4")
      .expect("Content-Type", /json/)
      .then((response) => {
        const { status, body } = response;
        expect(status).toEqual(200);
        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  test("Add a new task", (done) => {
    request(app)
      .post("/todo")
      .send({
        title: "New Task",
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(201)
      .then((response) => {
        const { body, status } = response;
        expect(body.message).toEqual("New Task Added");
        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });


  test("Update a task", (done) => {
    request(app)
      .put("/todo/2")
      .send({
        title: "New Task",
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const { body, status } = response;
        expect(body.message).toEqual("Task Updated");
        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  test("Delete a task", (done) => {
    request(app)
      .delete("/todo/3")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const { body, status } = response;
        expect(body.message).toEqual("Task Removed");
        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });
});


