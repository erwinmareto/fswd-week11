const { ToDoList } = require("../models");

const allTasks = async (req, res, next) => {
  try {
    const tasks = await ToDoList.findAll();
    console.log(tasks);
    res.status(200).json(tasks);
  } catch (error) {
    next(error);
  }
};

const detailTodo = async (req, res, next) => {
  try {
    const { id } = req.params;
    const task = await ToDoList.findByPk(id);
    if (!task) {
      throw { name: "NotFound" };
    }
    res.status(200).json(task);
  } catch (error) {
    next(error);
  }
};

const createTodo = async (req, res, next) => {
  try {
    const { title } = req.body;
    const task = await ToDoList.create({ title: title });
    res.status(201).json({ message: "New Task Added" });
  } catch (error) {
    next(error);
  }
};

const updateTodo = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { title } = req.body;
    const task = await ToDoList.update({ title: title }, {
      where:{
        id
      }
    });
    if (!task) {
      throw { name: 'NotFound' }
    }
    res.status(200).json({ message: "Task Updated" });
  } catch (error) {
    next(error);
  }
};

const deleteTodo = async (req, res, next) => {
  try {
    const { id } = req.params;
    const task = await ToDoList.destroy({
      where: {
        id,
      },
    });
    if (!task) {
      throw { name: "NotFound" };
    }
    res.status(200).json({ message: "Task Removed" });
  } catch (error) {
    next(error);
  }
};

module.exports = { allTasks, detailTodo, createTodo, updateTodo, deleteTodo };
