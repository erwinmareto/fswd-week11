'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('ToDoLists', [
      {
        title: 'Task 1',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 2',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 3',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 4',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 5',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 6',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 7',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 8',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 9',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 10',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ])
    
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('TodoLists', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
