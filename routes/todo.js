const express = require("express");
const router = express.Router();
const todoController = require("../controllers/todoController.js");

router.get("/", todoController.allTasks);
router.get("/:id", todoController.detailTodo);
router.post("/", todoController.createTodo);
router.put('/:id', todoController.updateTodo);
router.delete("/:id", todoController.deleteTodo);

module.exports = router;
