const express = require("express");
const app = express();
// const PORT = process.env.PORT || 3000;
const routes = require("./routes/index.js");
const errorHandler = require("./middlewares/errorHandler.js");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(routes);
app.use(errorHandler);

// app.listen(PORT, () => {
//   console.log(`Listening on port ${PORT}`);
// });

module.exports = app;
